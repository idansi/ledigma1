<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use backend\models\Standard;
use yii\behaviors\BlameableBehavior;
//use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\User;
use app\models\Status;
use app\models\Category;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $category
 * @property int $author
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['category', 'author', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
        ];

    return [
        [
            'class' => BlameableBehavior::className(),
            'createdByAttribute' => 'author_id',
            'updatedByAttribute' => 'updater_id',
        ],

    ];

    return [
        [
            'class' => TimestampBehavior::className(),
            'updatedAtAttribute' => 'update_time',
            'value' => new Expression('NOW()'),
        ],
    ];
    }


    public function getStatuses(){
        return $this->hasOne(Status::className(),['id'=>'status'] );
    }

    public function getCategoryies(){
        return $this->hasOne(Category::className(),['id'=>'category'] );
    }

    public function getAuthories(){
        return $this->hasOne(User::className(),['id'=>'author'] );
    }

    public function beforeSave($insert)
    {
        //preventing from users without editor or author permission
        //change from draft to publish
        if (parent::beforeSave($insert)) {
            if (\Yii::$app->user->can('editor')) {
                return true;            
            } else {
                if($this->status == 1){
                    return true;  
                } else {
                    return false;     
                }
            }
        }
        return false;
    } 
}
