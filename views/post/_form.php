<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\selectize\SelectizeTextInput;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Category;
use app\models\Status;
/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

     <?= $form->field($model, 'category')->dropDownList(
        ArrayHelper::map(Category::find()->asArray()->all(),'id','name')
       ) ?>

   
      <?= $form->field($model, 'author')->dropDownList(
        ArrayHelper::map(User::find()->asArray()->all(),'id','name')
       ) ?>

     <?= $form->field($model, 'status')->dropDownList(
        ArrayHelper::map(Status::find()->asArray()->all(),'id','name')
       ) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
